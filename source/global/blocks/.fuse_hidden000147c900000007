//colors
$white: white
$black_70: rgba(0, 0, 0, 0.7)
$color_blue_bell_approx: #a197cb
$color_emerald_50_approx: rgba(68, 207, 88, 0.5)
$color_emerald_approx: #40ce56
$black_50: rgba(0, 0, 0, 0.5)
$color_deluge_70_approx: rgba(115, 88, 155, 0.7)
$color_medium_purple_approx: #8a5ee5
$color_purple_heart_approx: #6533ca
$color_goblin_approx: #30863d

body
	min-height: 100%
	#video_hero_wrapper
		position: relative
		height: calc(100vh - 6rem)
		overflow: hidden
		+media($mobile)
			box-sizing: border-box
			height: 80rem
			width: 100%
			float: left
		video 
			position: absolute
			top:0
			bottom:0
			left: 0
			margin: 0
			padding: 0
			min-width: 100%
			max-width: 200%
			min-height: 100%
			width: auto
			+media($mobile)
				display: none
			//opacity: 0.7

		#main_box 
			position: absolute
			margin: 0
			padding-left: 2rem
			padding-right: 2rem
			padding-top: 2rem
			padding-bottom: 0
			top: 3rem
			left: 3rem
			width: 570px
			text-align: left
			color: $white
			background-color: $black_70
			+media($mobile)
				display: block
				position: relative
				width: 100%
				left: 0
				top: 0
				height: 20rem

			h3
				font-family: MetaPro-Normal
				font-size: 1.5rem
				line-height: 15px
				&:nth-child(1) 
					color: $white

				&:nth-child(2) 
					color: $color_blue_bell_approx
					font-weight: 900
			p
				font-family: HelveticaNeueLTPro-Lt
				font-weight: 600
				font-size: 0.9rem

			.firstw 
				font-weight: 900

			.secondw 
				font-style: italic


		#rigth_box 
			height: 100%
			+media($mobile)
				position: relative
			#top_box 
				position: absolute
				top: 0
				right: 0
				width: 50vh
				height: 50%
				background-color: $color_emerald_50_approx
				color: $white
				padding-left: 4rem
				padding-right: 4rem
				overflow: auto
				+media($mobile)
					width: 100%
					height: 23%

				h5
					background-color: $color_emerald_approx
					text-align: center
					margin-left: -4rem
					margin-right: -4rem
					position: relative
					padding-top: 1rem
					padding-bottom: 0.5rem
					font-weight: 900
					color: $white
					font-family: MetaPro-Normal
				p
					font-family: HelveticaNeueLTPro-Lt
					font-weight: 600
					font-size: 0.9rem
					margin-bottom: 1rem

				#pdf_btn_gray 
					position: relative
					button 
						border-radius: 0
						background-color: $black_50
						font-family: MetaPro-Normal
						font-weight: 900
						width: 100%
						text-align: left
						height: 40px
						line-height: initial
						transition: all 0.2s ease-in-out
						&:hover
							background-color: rgba(0, 0, 0, 0.7)
						i
							position: absolute
							right: 8px
							z-index: 1
							font-size: x-large
							bottom: -1px
						&:after 
							content: ""
							color: $white
							background-color: $black_70
							width: 40px
							height: 40px
							position: absolute
							right: 0
							font-size: xx-large
							text-align: center
							bottom: 0


			#bottom_box 
				position: absolute
				bottom: 0
				right: 0
				width: 50vh
				height: 50%
				background-color: $color_deluge_70_approx
				color: $white
				padding-left: 3rem
				padding-right: 3rem
				text-align: center
				overflow: visible
				box-sizing: border-box
				h5 
					text-align: center
					position: relative
					padding-top: 1rem
					font-weight: 900
					color: $white
					font-family: MetaPro-Normal
					padding-bottom: 5%
				ul 
					width: 100%
					li 
						display: block
						margin-bottom: .5rem
						width: 50%
						float: left
						img 
							margin: auto
							display: block
							padding-left: 1rem
							padding-right: 1rem
							padding-bottom: 0.9rem
							width: 70%

						span
							font-family: MetaPro-Normal
							font-weight: 900
							padding-bottom: 1rem
							font-size: 0.9rem

		#contact_btn 
			position: absolute
			bottom: 3rem
			left: 3rem
			button 
				border-radius: 0
				background-color: $color_medium_purple_approx
				width: 120%
				text-align: left
				height: 40px
				font-family: MetaPro-Normal
				font-weight: 900
				line-height: initial
				transition: all 0.2s ease-in-out
				&:hover
					background-color: #996FEF

				&:after 
					content: "+"
					color: $white
					background-color: $color_purple_heart_approx
					width: 40px
					height: 40px
					position: absolute
					right: -58px
					font-size: xx-large
					text-align: center
					bottom: 0
					line-height: 2.5rem

		#pdf_btn_green 
			position: absolute
			bottom: 3rem
			left: 30rem
			+media(max-width 1024px)
				bottom: 0.28rem
				left: 3rem
			button 
				border-radius: 0
				background-color: $color_emerald_approx
				font-weight: 100
				width: 120%
				text-align: left
				height: 40px
				font-family: MetaPro-Normal
				font-weight: 900
				line-height: initial
				transition: all 0.2s ease-in-out
				+media(max-width 1024px)
					width: 110%
				&:hover
					background-color: #48E860
				i
					position: absolute
					right: -55px
					z-index: 1
					font-size: x-large
					bottom: -1px
					+media(max-width 1024px)
						right: -45px
				&:after 
					content: ""
					color: $white
					background-color: $color_goblin_approx
					width: 40px
					height: 40px
					position: absolute
					right: -64px
					font-size: xx-large
					text-align: center
					bottom: 0
					+media(max-width 1024px)
						right: -53px