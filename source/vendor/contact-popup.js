$(document).ready(function() {

  $(".nav-link:last-child, .footer_links:last-child, #contact_btn > a").click(function() {

    $(".popup").fadeToggle("fast");

  });

  $(".inner img").click(function() {

    $(".popup").fadeOut("fast");

  });

//remove all &nbsp from h2's it is originaly a bug from compiler
  $(".text-full").each(function() {

    var $this = $(this);

    $this.html($this.html().replace(/(&nbsp;)*/g,"");

  });
    
});